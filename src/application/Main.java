package application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import javafx.application.Application;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("Vista.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.getIcons().add(new Image("/iconoapp.png"));
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.setTitle("LionDownloaderYT");
			primaryStage.show();
			centrarVentana(primaryStage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			File youtubeDlFile = new File(System.getProperty("java.io.tmpdir") + "youtube-dl.exe");
			File ffmpegEXEFile = new File(System.getProperty("java.io.tmpdir") + "ffmpeg.exe");

			// Comprobamos si el archivo youtube-dl.exe y ffmpeg.exe existe en la carpeta de temporales del usuario
			if (!youtubeDlFile.exists() || !ffmpegEXEFile.exists()) {
				// Copiamos el archivo youtube-dl.exe a los temporales del equipo
				InputStream streamYoutube = Main.class.getClassLoader().getResourceAsStream("youtube-dl.exe");
				try (FileOutputStream fos = new FileOutputStream(
						System.getProperty("java.io.tmpdir") + "youtube-dl.exe")) {
					byte[] buf = new byte[2048];
					int r;
					while (-1 != (r = streamYoutube.read(buf))) {
						fos.write(buf, 0, r);
					}
				}

				// Copiamos el archivo ffmpeg.exe a los temporales del equipo
				InputStream streamFfmpeg = Main.class.getClassLoader().getResourceAsStream("ffmpeg.exe");
				try (FileOutputStream fos = new FileOutputStream(System.getProperty("java.io.tmpdir") + "ffmpeg.exe")) {
					byte[] buf = new byte[2048];
					int r;
					while (-1 != (r = streamFfmpeg.read(buf))) {
						fos.write(buf, 0, r);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		launch(args);
	}
	
	/**
	 * Función para centrar la ventana
	 * @param primaryStage
	 */
	public static void centrarVentana(Stage primaryStage) {
		Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
		primaryStage.setX((primScreenBounds.getWidth() - primaryStage.getWidth()) / 2);
        primaryStage.setY((primScreenBounds.getHeight() - primaryStage.getHeight()) / 2);
	}
}
