package application;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 * Clase controladora de la interfaz
 * 
 * @author DIEGO LITTLELION
 *
 */
public class Controlador implements Initializable {
	private String ruta_temp = System.getProperty("java.io.tmpdir");
	private String rutaDestinoDescarga;

	@FXML
	private AnchorPane raiz;
	@FXML
	private Button descargarBoton;
	@FXML
	private TextField urlTF;
	@FXML
	private CheckBox videoCB;
	@FXML
	private CheckBox audioCB;
	@FXML
	private ImageView urlIMG;
	@FXML
	private Text mensaje;

	/**
	 * Función que se lanza al clickar el botón "Destino"
	 */
	@FXML
	public void seleccionDirectorio() {
		try {
			Stage stage = (Stage) descargarBoton.getScene().getWindow();
			DirectoryChooser seleccionDirectorio = new DirectoryChooser();
			File directorioSeleccionado = seleccionDirectorio.showDialog(stage);
			rutaDestinoDescarga = directorioSeleccionado.getCanonicalPath();
			descargar();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Función que se lanza al clickar el botón "Descargar"
	 */
	@FXML
	public void descargar() {
		String url = urlTF.getText();

		if (videoCB.isSelected() && url.length() > 0) {
			new HiloDescargador("video", url, rutaDestinoDescarga, ruta_temp, raiz, mensaje).start();
		}

		if (audioCB.isSelected() && url.length() > 0) {
			new HiloDescargador("audio", url, rutaDestinoDescarga, ruta_temp, raiz, mensaje).start();
		}

		if (!videoCB.isSelected() && !audioCB.isSelected()) {
			mostrarMensaje("¡Debes elegir si quieres descargar el vídeo o el audio!");
		} else if (url.length() == 0) {
			mostrarMensaje("¡Debes introducir un vídeo!");
		}
	}

	/**
	 * Función para mostrar una alerta
	 * 
	 * @param mensaje Mensaje que se mostrará en la alerta
	 */
	public void mostrarMensaje(String texto) {
		mensaje.setText(texto);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		String[] command = { "cmd", };
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
			new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			stdin.println(ruta_temp + "youtube-dl -U");
			stdin.close();
			p.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
