package application;

import java.io.PrintWriter;

import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

public class HiloDescargador extends Thread {
	private AnchorPane raiz;
	private Text mensaje;
	private String peticion;
	private String url;
	private String rutaDestinoDescarga;
	private String ruta_temp;

	public HiloDescargador(String peticion, String url, String rutaDestinoDescarga, String ruta_temp, AnchorPane raiz, Text mensaje) {
		this.peticion = peticion;
		this.url = url;
		this.rutaDestinoDescarga = rutaDestinoDescarga;
		this.ruta_temp = ruta_temp;
		this.raiz = raiz;
		this.mensaje = mensaje;
	}

	public void run() {
		// Bloqueamos la interfaz para que el usuario no pueda usarla mientras se descarga un archivo
		raiz.setDisable(true);
		mensaje.setText("Descargando...");
		
		String[] command = { "cmd", };
		Process p;
		
		try {
			p = Runtime.getRuntime().exec(command);
			new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
			new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			stdin.println("cd \"" + ruta_temp + "\"");
			
			if (peticion.equals("video")) {
				stdin.println(ruta_temp + "youtube-dl " + url + " -f bestvideo[ext=mp4]+bestaudio[ext=m4a] -o "
						+ rutaDestinoDescarga + "\\%(title)s.mp4");
			} else if (peticion.equals("audio")) {
				stdin.println(ruta_temp + "\\youtube-dl -x --extract-audio --audio-format mp3 -o \""
						+ rutaDestinoDescarga + "\"\\%(title)s.mp3' " + url);
			}
			
			stdin.close();
			p.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		raiz.setDisable(false);
		mensaje.setText("¡Descarga completada!");
	}
}
